package BillPaymentWS.CIMB3rdParty

import com.mashape.unirest.http.Unirest
import org.xml.sax.InputSource
import java.io.StringReader
import java.util.HashMap
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.soap.Node
import javax.xml.parsers.DocumentBuilder




    private val payment = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"https://directchannel.cimbniaga.co.id:8002\" \n" +
            "xmlns:java=\"java:prismagateway.service.HostCustomer\">" +
            "   <soapenv:Header/>" +
            "   <soapenv:Body>" +
            "       <ns:HostCustomer>" +
            "           <ns:input>\n" +
            "               <java:tokenAuth>0314901bbe6790444622494b171a86c11de1a7bb6e014cbf07f7a4809664b716</java:tokenAuth>\n" +
            "               <java:txnData><![CDATA[\n" +
            "                   <transferRequest>\n" +
            "                   <transfer>\n" +
            "                       <transferId>99020160215151446</transferId>\n" +
            "                       <txnDate>20160215</txnDate>\n" +
            "                       <debitAcctNo>800028582000</debitAcctNo>\n" +
            "                       <benAcctNo>1451284636</benAcctNo>\n" +
            "                       <benName>Wahyu Adhi Laksana</benName>\n" +
            "                       <benAddr1>Alamat 1</benAddr1>\n" +
            "                       <benAddr2>Alamat 2</benAddr2>\n" +
            "                       <benBankCode>014</benBankCode>\n" +
            "                       <benBankRTGS></benBankRTGS>\n" +
            "                       <benBankName>BCA</benBankName>\n" +
            "                       <benBankAddr></benBankAddr>\n" +
            "                       <benBankCity>JAKARTA</benBankCity>\n" +
            "                       <benBankCountry>INDONESIA</benBankCountry>\n" +
            "                       <currAmount>IDR</currAmount>\n" +
            "                       <amount>10001</amount>\n" +
            "                       <memo>ATM_TRANSFER</memo>\n" +
            "                       <instructDate>20160215</instructDate>\n" +
            "                       <remitID>99020160215151446</remitID>\n" +
            "                       <remitName>Wahyu Adhi Laksana</remitName>\n" +
            "                       <remitLoc></remitLoc>\n" +
            "                   </transfer>\n" +
            "                   <charge>\n" +
            "                   <debitBankCharge></debitBankCharge>\n" +
            "                   <debitAgentCharge></debitAgentCharge>\n" +
            "                   </charge>\n" +
            "                   <filler>\n" +
            "                       <filler1></filler1>\n" +
            "                       <filler2></filler2>\n" +
            "                       <filler3></filler3>\n" +
            "                   </filler>\n" +
            "                   <lld>\n" +
            "                       <isLLD></isLLD>\n" +
            "                       <purpose></purpose>\n" +
            "                       <benStatus>02</benStatus>\n" +
            "                       <benCategory>1</benCategory>\n" +
            "                       <remStatus></remStatus>\n" +
            "                       <remCategory></remCategory>\n" +
            "                       <transRel></transRel>\n" +
            "                   </lld>\n" +
            "                   <advice>\n" +
            "                       <beneficiaryEmail></beneficiaryEmail>\n" +
            "                       <remitterEmail></remitterEmail>\n" +
            "                       <adviceDetail></adviceDetail>\n" +
            "                   </advice>\n" +
            "                   </transferRequest>]]>\n" +
            "                   </java:txnData><java:serviceCode>ATM_TRANSFER</java:serviceCode><java:corpID>ID9999BIZCH</java:corpID>\n" +
            "                   <java:requestID>1862619247</java:requestID><java:txnRequestDateTime>20160215151447</java:txnRequestDateTime>" +
            "           </ns:input>\n" +
            "       </ns:HostCustomer>" +
            "   </soapenv:Body>" +
            "</soapenv:Envelope>"

    private val payment2 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://10.25.143.133\" xmlns:java=\"java:prismagateway.service.HostCustomer\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "       <ns:HostCustomer>\n" +
            "           <ns:input>\n" +
            "               <java:tokenAuth>9fa0e2bf999bad32abe9304362ccd5fc56e9e5c2bda8dd081a048299f71e4143</java:tokenAuth>\n" +
            "               <java:txnData>\n" +
            "               <![CDATA[\n" +
            "                   <transferRequest>\n" +
            "                       <transfer>\n" +
            "                           <transferId>99020170427191607</transferId>\n" +
            "                           <txnDate>20170427</txnDate>\n" +
            "                           <debitAcctNo>800099839000</debitAcctNo>\n" +
            "                           <benAcctNo>800099863300</benAcctNo>\n" +
            "                           <benName>WILIAM</benName>\n" +
            "                           <benBankName>CIMB Niaga</benBankName>\n" +
            "                           <benBankAddr1></benBankAddr1>\n" +
            "                           <benBankAddr2></benBankAddr2>\n" +
            "                           <benBankAddr3></benBankAddr3>\n" +
            "                           <benBankBranch></benBankBranch>\n" +
            "                           <benBankCode>022</benBankCode>\n" +
            "                           <benBankSWIFT></benBankSWIFT>\n" +
            "                           <currCd>IDR</currCd>\n" +
            "                           <amount>10002</amount>\n" +
            "                           <memo>ACCOUNT TRANSFER TEST</memo>\n" +
            "                       </transfer>\n" +
            "                   </transferRequest>\n" +
            "               ]]>\n" +
            "               </java:txnData>\n" +
            "               <java:serviceCode>ACCOUNT_TRANSFER</java:serviceCode>\n" +
            "               <java:corpID>ID9999BIZSLO</java:corpID>\n" +
            "               <java:requestID>1767111607</java:requestID>\n" +
            "               <java:txnRequestDateTime>20170427191607</java:txnRequestDateTime>\n" +
            "           </ns:input>\n" +
            "       </ns:HostCustomer>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>"

    private val payment3 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:bil=\"http://CIMB3rdParty/BillPaymentWS\">\n" +
            "   <soapenv:Header/>\n" +
            "   <soapenv:Body>\n" +
            "      <bil:CIMB3rdParty_EchoRq>\n" +
            "         <bil:EchoRequest>as</bil:EchoRequest>\n" +
            "      </bil:CIMB3rdParty_EchoRq>\n" +
            "   </soapenv:Body>\n" +
            "</soapenv:Envelope>"

    fun main(args: Array<String>) {
        val jsonResponse = Unirest.post("http://localhost:7063/").header("Content-Type","text/xml")
                .body(payment3).asString()

        val factory = DocumentBuilderFactory.newInstance()
        val builder = factory.newDocumentBuilder()
        val document = builder.parse(InputSource(StringReader(jsonResponse.body)))
        val n = document.firstChild
        val nl = n.childNodes
        var an: org.w3c.dom.Node
        var an2: org.w3c.dom.Node
        var an3: org.w3c.dom.Node

        val tagNode = HashMap<String, String>()
        for (i in 0..nl.length - 1) {
            an = nl.item(i)
            if (an.nodeType == Node.ELEMENT_NODE) {
                val nl2 = an.childNodes
                for (i2 in 0..nl2.length - 1) {
                    an2 = nl2.item(i2)
                    val nl3 = an2.childNodes
                    for (i3 in 0..nl3.length - 1) {
                        an3 = nl3.item(i3)
                        var sibling: org.w3c.dom.Node? = an3.firstChild
                        //input first child node
                        if (sibling != null) tagNode.put(sibling.nodeName, sibling.textContent)
                        while (sibling != null) {
                            try {
                                //iterate next child
                                sibling = sibling.nextSibling
                                tagNode.put(sibling!!.nodeName, sibling.textContent)
                            } catch (e: NullPointerException) {
                                continue
                            }
                        }
                    }
                }
            }
        }

        print(tagNode.get("CIMB3rdParty_EchoRs"))
    }
